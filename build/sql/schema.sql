
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- users
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(255) NOT NULL,
    `password` VARCHAR(32) NOT NULL,
    `group` TINYINT NOT NULL,
    `create_time` DATE,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- routes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `routes`;

CREATE TABLE `routes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(600),
    `short_desciption` VARCHAR(2000),
    `desciption` TEXT,
    `origin` VARCHAR(600),
    `image` VARCHAR(1000),
    `create_time` DATE,
    `user_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `routes_FI_1` (`user_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- markers
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `markers`;

CREATE TABLE `markers`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(1000),
    `id_marker` VARCHAR(1000),
    `desciption` TEXT,
    `order` TINYINT,
    `included` TINYINT(1),
    `icon` VARCHAR(255),
    `lat` REAL NOT NULL,
    `lng` REAL NOT NULL,
    `create_time` DATE,
    `routes_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `markers_FI_1` (`routes_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- images
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `images`;

CREATE TABLE `images`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `url` VARCHAR(1000) NOT NULL,
    `name` VARCHAR(600),
    `create_time` DATE,
    `marker_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `images_FI_1` (`marker_id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
