<?php

define("APP_NAME", "Routes");
if (isset($_SERVER['HTTPS'])) {
    define("HTTP_TYPE", "https://");
} else {
    define("HTTP_TYPE", "http://");
}
$config = array(
    "db" => array(
        "dbname" => "maps",
        "username" => "root",
        "password" => "caca66",
        "host" => "localhost")
    ,
    "appNane" => "Routes",
    "urls" => array(
        "baseUrl" => "http://localhost/Routes",
        "componentsUrl" => HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/",
        "vendorUrl" => HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/vendor/",
        "templateUrl" => HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/templates/",
        "mapsUrl" => HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/maps/",
        "loginUrl" => HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/login",
        "securityFileUrl" => HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/login/security.php",
    ),
    "pathsAbs" => array(
        "componentsDir" => $_SERVER['DOCUMENT_ROOT'] ."/" . APP_NAME . "/components/",
        "vendorDir" => $_SERVER['DOCUMENT_ROOT']."/" . APP_NAME .  "/vendor/",
        "templateDir" => $_SERVER['DOCUMENT_ROOT']."/" . APP_NAME .  "/components/templates/",
        "mapsDir" => $_SERVER['DOCUMENT_ROOT'] ."/". APP_NAME .  "/components/maps/",
        "loginDir" => $_SERVER['DOCUMENT_ROOT'] ."/" . APP_NAME .  "/components/login/",
        "errorPage" => $_SERVER['DOCUMENT_ROOT'] ."/". APP_NAME . "/components/login/error.php",
        "securityPage" => $_SERVER['DOCUMENT_ROOT']."/". APP_NAME .  "/components/login/security.php",
        "controllerPage" => $_SERVER['DOCUMENT_ROOT']."/". APP_NAME .  "/build/controller.php",
        "buildDir" => $_SERVER['DOCUMENT_ROOT']."/". APP_NAME .  "/build/"
    ),
    "paths" => array(
        "componentsDir" => "/" .  APP_NAME  ."/" . "/components/",
        "vendorDir" => "/" .  APP_NAME . "/vendor/",
        "templateDir" => "/" . APP_NAME . "/components/templates/",
        "mapsDir" => "/" .  APP_NAME . "/components/maps/",
        "loginDir" => "/" .  APP_NAME . "/components/login/",
        "errorPage" => "/" .  APP_NAME ."/components/login/error.php",
        "securityPage" => "components/login/security.php",
        "controllerPage" => "/" .  APP_NAME ."/build/controller.php",
        
    )
);

$arPaths[] = get_include_path();
//recuperamos la ruta que tiene almacenada por defecto.
//guardamos las rutas personalizadas.  Para este ejemplo solo utilizaré
//los modelos controladores y vistas
$arPaths[] = $config['paths']['componentsDir'];
$arPaths[] = $config['paths']['loginDir'];
$arPaths[] = $config['paths']['mapsDir'];
 
$sMergedPaths = implode(PATH_SEPARATOR,$arPaths);
set_include_path($sMergedPaths);







/*

if (isset($_SERVER['HTTPS'])) {
    define("HTTP_TYPE", "https://");
} else {
    define("HTTP_TYPE", "http://");
}
define("APP_NAME", "Routes");
define("COMPONENT_DIR", HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/");
define("LOGIN_DIR", HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/login");
define("MAPS_DIR", HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/maps/");
define("VENDOR_DIR", HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/vendor/");
define("TEMPLATE_DIR", HTTP_TYPE . $_SERVER['SERVER_NAME'] . "/" . APP_NAME . "/components/templates/");
?>*/

