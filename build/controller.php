<?php

// Cargamos dependencias de Propel
include_once 'config.php';
require_once $config['pathsAbs']['vendorDir'] . '/propel/runtime/lib/Propel.php';
require_once $config['pathsAbs']['componentsDir'] . 'FirePHPCore/FirePHP.class.php';
Propel::init($config['pathsAbs']['buildDir'] . "conf/maps-conf.php");
set_include_path($config['pathsAbs']['buildDir'] . "classes" . PATH_SEPARATOR . get_include_path());
ob_start();

if ($_POST) {
    $xhs = $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
    //Comprobamos que la petición se ha hecho desde AJAX
    if (!$xhs) {
        header('HTTP/1.1 500 Error: Request must come from  Ajax');
        exit('HTTP/1.1 500 Error: Request must come from  Ajax');
    } else {
        //Sacamos la accion realizada por el usuario y filtramos por ella
        $action = $_POST['action'];
        //Si estamos haciendo login
        if ($action == "login") {
            if (isset($_POST['user']) || isset($_POST['pass'])) {

                $userString = filter_var($_POST['user'], FILTER_SANITIZE_STRING);
                $passString = md5(filter_var($_POST['pass'], FILTER_SANITIZE_STRING));
                $user = UserQuery::create()->filterByUsername($userString)->findOne();

                if (isset($user)) {
                    if ($passString == $user->getPassword()) {
                        $message = "Accepted";
                        session_start();
                        $_SESSION['userId'] = $user->getId();
                        $_SESSION['user'] = $user->toArray();
                        $routes = RouteQuery::create()->filterByUserId($user->getId())->orderById()->find();
                        if ($routes->count() > 0) {
                            $output = $routes->toJSON();
                        } else {
                            $output = "null";
                        }
                        $_SESSION['routes'] = $output;

                        exit($output);
                    } else {
                        $message = "Password Error";
                        exit($message);
                    }
                } else {
                    $message = "Usuario Desconocido";
                    exit($message);
                }
            }
        }
        if ($action == "createUser") {
            $userString = filter_var($_POST['user'], FILTER_SANITIZE_STRING);
            $passString = md5(filter_var($_POST['pass'], FILTER_SANITIZE_STRING));
            $user = UserQuery::create()->filterByUsername($userString)->findOne();
            if (isset($user)) {
                $message = "El usuario ya existe";
                exit($message);
            } else {
                $user = new User();
                $user->setUsername($userString);
                $user->setPassword($passString);
                $user->save();
                $message = "Usuario creado";
                exit($message);
            }
        }

        if ($action == 'storeMarkers') {
            session_start();
            $routeId = $_POST['routeId'];
            $markers = $_POST['marcadores'];
            updateRoutesGlobal($_SESSION['userId']);
            deleteAllMarkersFromRoute($routeId);
            saveMarkers($markers, $routeId);
            $_SESSION['markers'] = $markers;

            exit('Marker Edited');
        }
        if ($action == "newRoute") {
            session_start();
            $firephp = FirePHP::getInstance(true);
            $newRoute = saveRoute($_POST['name'], $_POST['description'], $_POST['image'], $_POST['origin'], $_POST['short'], $_SESSION['userId']);
            $firephp->log($newRoute, 'Nueva Ruta');
            updateRoutesGlobal($_SESSION['userId']);
        }

        if ($action == 'editRoute') {
            session_start();
            $routeId = $_POST['id'];
            $updatedRoute = editRoute($routeId, $_POST['name'], $_POST['description'], $_POST['image'], $_POST['origin'], $_POST['short'], $_SESSION['userId']);
            updateRoutesGlobal($_SESSION['userId']);
            exit();
        }


        if ($action == 'deleteRoute') {
            session_start();
            $id = $_POST['id'];
            $route = RouteQuery::create()->findPk($id);
            $route->delete();
            $routes = RouteQuery::create()->filterByUserId($_SESSION['userId'])->orderById()->find();
            if ($routes->count() > 0) {
                $output = $routes->toJSON();
            } else {
                $output = "null";
            }
            $_SESSION['routes'] = $output;
            exit($output);
        }
    }
}

if ($_GET) {
    $action = $_GET['action'];


    if ($action == "editRoute") {
        $firephp = FirePHP::getInstance(true);
        $actualRoute = $_GET['id'];
        $origin = $_GET['origin'];
        $markers = MarkerQuery::create()->filterByRoutesId($actualRoute)->orderById()->find();
        if ($markers->count() > 0) {
            $output = $markers->toJSON();
            $_SESSION['markers'] = $output;
        } else {
            $output = "null";
            $_SESSION['markers'] = $output;
        }
        $_SESSION['markers'] = $output;
        $_SESSION['origin'] = $origin;
    }
}

//FUNCIONES

function saveRoute($name, $description, $image, $origin, $short, $userId) {
    $newRoute = new Route();
    $newRoute->setName($name[0]);
    $newRoute->setDesciption($description[0]);
    $newRoute->setImage($image);
    $newRoute->setUserId($userId);
    $newRoute->setShortDesciption($short[0]);
    $newRoute->setOrigin($origin);
    $newRoute->save();
    return $newRoute;
}

function editRoute($routeId, $name, $description, $image, $origin, $short, $userId) {
    $route = RouteQuery::create()->findPk($routeId);
    $route->setName($name[0]);
    $route->setDesciption($description[0]);
    $route->setImage($image);
    $route->setShortDesciption($short[0]);
    $route->setOrigin($origin);

    $route->setUserId($userId);
    $route->save();
    return $route;
}

function updateRoutesGlobal($userId) {
    $routes = RouteQuery::create()->filterByUserId($userId)->orderById()->find();
    if ($routes->count() > 0) {
        $output = $routes->toJSON();
    } else {
        $output = "null";
    }
    $_SESSION['routes'] = $output;
    return $output;
}

function deleteAllMarkersFromRoute($activeRoute) {
    $firephp = FirePHP::getInstance(true);
    $firephp->log($activeRoute, 'Active Route');
    $marks = MarkerQuery::create()->filterByRoutesId($activeRoute)->delete();

    $firephp->log($marks, 'Borrados');
}

function saveMarkers($markers, $activeRoute) {
    $firephp = FirePHP::getInstance(true);
    $firephp->log($markers, 'Markers');
    foreach ($markers as $marker) {
        
        $name = $marker['title'];
        $firephp->log($name, '$name');
        $idMarker = $marker['id'];
        $createDate =$marker['createDate'];
        $routeId = $activeRoute;
        $included = $marker['included'];
        $icon = $marker['icon'];
        $description = $marker['description'];
        $firephp->log($description, '$description');
        $order = $marker['order'];
        $lat = $marker['lat'];
        $lng = $marker['lng'];
        $firephp->log($lat, '$lat');
        $firephp->log($lng, '$lng');
        // CREAR FUNCION PARA ESTO EN LAS CLASES DE PROPEL
        $newMarker = new Marker();
        $newMarker->setTitle($name);
        $newMarker->setCreateTime($createDate);
        $newMarker->setLat($lat);
        $newMarker->setLng($lng);
        $newMarker->setOrder($order);
        $newMarker->setDesciption($description);
        $newMarker->setIcon($icon);
        $newMarker->setRoutesId($routeId);
        $newMarker->setIncluded($included);
        $newMarker->setIdMarker($idMarker);
        $newMarker->save();
    }
}
