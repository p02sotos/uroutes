<?php
// This file generated by Propel 1.7.1 convert-conf target
return array (
  'BaseImage' => 'maps/om/BaseImage.php',
  'BaseImagePeer' => 'maps/om/BaseImagePeer.php',
  'BaseImageQuery' => 'maps/om/BaseImageQuery.php',
  'BaseMarker' => 'maps/om/BaseMarker.php',
  'BaseMarkerPeer' => 'maps/om/BaseMarkerPeer.php',
  'BaseMarkerQuery' => 'maps/om/BaseMarkerQuery.php',
  'BaseRoute' => 'maps/om/BaseRoute.php',
  'BaseRoutePeer' => 'maps/om/BaseRoutePeer.php',
  'BaseRouteQuery' => 'maps/om/BaseRouteQuery.php',
  'BaseUser' => 'maps/om/BaseUser.php',
  'BaseUserPeer' => 'maps/om/BaseUserPeer.php',
  'BaseUserQuery' => 'maps/om/BaseUserQuery.php',
  'Image' => 'maps/Image.php',
  'ImagePeer' => 'maps/ImagePeer.php',
  'ImageQuery' => 'maps/ImageQuery.php',
  'ImageTableMap' => 'maps/map/ImageTableMap.php',
  'Marker' => 'maps/Marker.php',
  'MarkerPeer' => 'maps/MarkerPeer.php',
  'MarkerQuery' => 'maps/MarkerQuery.php',
  'MarkerTableMap' => 'maps/map/MarkerTableMap.php',
  'Route' => 'maps/Route.php',
  'RoutePeer' => 'maps/RoutePeer.php',
  'RouteQuery' => 'maps/RouteQuery.php',
  'RouteTableMap' => 'maps/map/RouteTableMap.php',
  'User' => 'maps/User.php',
  'UserPeer' => 'maps/UserPeer.php',
  'UserQuery' => 'maps/UserQuery.php',
  'UserTableMap' => 'maps/map/UserTableMap.php',
);