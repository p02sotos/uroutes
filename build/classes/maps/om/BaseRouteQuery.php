<?php


/**
 * Base class that represents a query for the 'routes' table.
 *
 *
 *
 * @method RouteQuery orderById($order = Criteria::ASC) Order by the id column
 * @method RouteQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method RouteQuery orderByShortDesciption($order = Criteria::ASC) Order by the short_desciption column
 * @method RouteQuery orderByDesciption($order = Criteria::ASC) Order by the desciption column
 * @method RouteQuery orderByOrigin($order = Criteria::ASC) Order by the origin column
 * @method RouteQuery orderByImage($order = Criteria::ASC) Order by the image column
 * @method RouteQuery orderByCreateTime($order = Criteria::ASC) Order by the create_time column
 * @method RouteQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 *
 * @method RouteQuery groupById() Group by the id column
 * @method RouteQuery groupByName() Group by the name column
 * @method RouteQuery groupByShortDesciption() Group by the short_desciption column
 * @method RouteQuery groupByDesciption() Group by the desciption column
 * @method RouteQuery groupByOrigin() Group by the origin column
 * @method RouteQuery groupByImage() Group by the image column
 * @method RouteQuery groupByCreateTime() Group by the create_time column
 * @method RouteQuery groupByUserId() Group by the user_id column
 *
 * @method RouteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RouteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RouteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RouteQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method RouteQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method RouteQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method RouteQuery leftJoinMarker($relationAlias = null) Adds a LEFT JOIN clause to the query using the Marker relation
 * @method RouteQuery rightJoinMarker($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Marker relation
 * @method RouteQuery innerJoinMarker($relationAlias = null) Adds a INNER JOIN clause to the query using the Marker relation
 *
 * @method Route findOne(PropelPDO $con = null) Return the first Route matching the query
 * @method Route findOneOrCreate(PropelPDO $con = null) Return the first Route matching the query, or a new Route object populated from the query conditions when no match is found
 *
 * @method Route findOneByName(string $name) Return the first Route filtered by the name column
 * @method Route findOneByShortDesciption(string $short_desciption) Return the first Route filtered by the short_desciption column
 * @method Route findOneByDesciption(string $desciption) Return the first Route filtered by the desciption column
 * @method Route findOneByOrigin(string $origin) Return the first Route filtered by the origin column
 * @method Route findOneByImage(string $image) Return the first Route filtered by the image column
 * @method Route findOneByCreateTime(string $create_time) Return the first Route filtered by the create_time column
 * @method Route findOneByUserId(int $user_id) Return the first Route filtered by the user_id column
 *
 * @method array findById(int $id) Return Route objects filtered by the id column
 * @method array findByName(string $name) Return Route objects filtered by the name column
 * @method array findByShortDesciption(string $short_desciption) Return Route objects filtered by the short_desciption column
 * @method array findByDesciption(string $desciption) Return Route objects filtered by the desciption column
 * @method array findByOrigin(string $origin) Return Route objects filtered by the origin column
 * @method array findByImage(string $image) Return Route objects filtered by the image column
 * @method array findByCreateTime(string $create_time) Return Route objects filtered by the create_time column
 * @method array findByUserId(int $user_id) Return Route objects filtered by the user_id column
 *
 * @package    propel.generator.maps.om
 */
abstract class BaseRouteQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRouteQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'maps';
        }
        if (null === $modelName) {
            $modelName = 'Route';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RouteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RouteQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RouteQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RouteQuery) {
            return $criteria;
        }
        $query = new RouteQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Route|Route[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RoutePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RoutePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Route A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Route A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `name`, `short_desciption`, `desciption`, `origin`, `image`, `create_time`, `user_id` FROM `routes` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Route();
            $obj->hydrate($row);
            RoutePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Route|Route[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Route[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RoutePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RoutePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RoutePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RoutePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RoutePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RoutePeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the short_desciption column
     *
     * Example usage:
     * <code>
     * $query->filterByShortDesciption('fooValue');   // WHERE short_desciption = 'fooValue'
     * $query->filterByShortDesciption('%fooValue%'); // WHERE short_desciption LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shortDesciption The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByShortDesciption($shortDesciption = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shortDesciption)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $shortDesciption)) {
                $shortDesciption = str_replace('*', '%', $shortDesciption);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RoutePeer::SHORT_DESCIPTION, $shortDesciption, $comparison);
    }

    /**
     * Filter the query on the desciption column
     *
     * Example usage:
     * <code>
     * $query->filterByDesciption('fooValue');   // WHERE desciption = 'fooValue'
     * $query->filterByDesciption('%fooValue%'); // WHERE desciption LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desciption The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByDesciption($desciption = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desciption)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desciption)) {
                $desciption = str_replace('*', '%', $desciption);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RoutePeer::DESCIPTION, $desciption, $comparison);
    }

    /**
     * Filter the query on the origin column
     *
     * Example usage:
     * <code>
     * $query->filterByOrigin('fooValue');   // WHERE origin = 'fooValue'
     * $query->filterByOrigin('%fooValue%'); // WHERE origin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $origin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByOrigin($origin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($origin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $origin)) {
                $origin = str_replace('*', '%', $origin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RoutePeer::ORIGIN, $origin, $comparison);
    }

    /**
     * Filter the query on the image column
     *
     * Example usage:
     * <code>
     * $query->filterByImage('fooValue');   // WHERE image = 'fooValue'
     * $query->filterByImage('%fooValue%'); // WHERE image LIKE '%fooValue%'
     * </code>
     *
     * @param     string $image The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByImage($image = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($image)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $image)) {
                $image = str_replace('*', '%', $image);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RoutePeer::IMAGE, $image, $comparison);
    }

    /**
     * Filter the query on the create_time column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateTime('2011-03-14'); // WHERE create_time = '2011-03-14'
     * $query->filterByCreateTime('now'); // WHERE create_time = '2011-03-14'
     * $query->filterByCreateTime(array('max' => 'yesterday')); // WHERE create_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $createTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByCreateTime($createTime = null, $comparison = null)
    {
        if (is_array($createTime)) {
            $useMinMax = false;
            if (isset($createTime['min'])) {
                $this->addUsingAlias(RoutePeer::CREATE_TIME, $createTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createTime['max'])) {
                $this->addUsingAlias(RoutePeer::CREATE_TIME, $createTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RoutePeer::CREATE_TIME, $createTime, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id >= 12
     * $query->filterByUserId(array('max' => 12)); // WHERE user_id <= 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(RoutePeer::USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(RoutePeer::USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RoutePeer::USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RouteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                ->addUsingAlias(RoutePeer::USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RoutePeer::USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', 'UserQuery');
    }

    /**
     * Filter the query by a related Marker object
     *
     * @param   Marker|PropelObjectCollection $marker  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RouteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByMarker($marker, $comparison = null)
    {
        if ($marker instanceof Marker) {
            return $this
                ->addUsingAlias(RoutePeer::ID, $marker->getRoutesId(), $comparison);
        } elseif ($marker instanceof PropelObjectCollection) {
            return $this
                ->useMarkerQuery()
                ->filterByPrimaryKeys($marker->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMarker() only accepts arguments of type Marker or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Marker relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function joinMarker($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Marker');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Marker');
        }

        return $this;
    }

    /**
     * Use the Marker relation Marker object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   MarkerQuery A secondary query class using the current class as primary query
     */
    public function useMarkerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinMarker($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Marker', 'MarkerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Route $route Object to remove from the list of results
     *
     * @return RouteQuery The current query, for fluid interface
     */
    public function prune($route = null)
    {
        if ($route) {
            $this->addUsingAlias(RoutePeer::ID, $route->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
