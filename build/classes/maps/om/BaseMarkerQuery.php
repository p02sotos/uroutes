<?php


/**
 * Base class that represents a query for the 'markers' table.
 *
 *
 *
 * @method MarkerQuery orderById($order = Criteria::ASC) Order by the id column
 * @method MarkerQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method MarkerQuery orderByIdMarker($order = Criteria::ASC) Order by the id_marker column
 * @method MarkerQuery orderByDesciption($order = Criteria::ASC) Order by the desciption column
 * @method MarkerQuery orderByOrder($order = Criteria::ASC) Order by the order column
 * @method MarkerQuery orderByIncluded($order = Criteria::ASC) Order by the included column
 * @method MarkerQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 * @method MarkerQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method MarkerQuery orderByLng($order = Criteria::ASC) Order by the lng column
 * @method MarkerQuery orderByCreateTime($order = Criteria::ASC) Order by the create_time column
 * @method MarkerQuery orderByRoutesId($order = Criteria::ASC) Order by the routes_id column
 *
 * @method MarkerQuery groupById() Group by the id column
 * @method MarkerQuery groupByTitle() Group by the title column
 * @method MarkerQuery groupByIdMarker() Group by the id_marker column
 * @method MarkerQuery groupByDesciption() Group by the desciption column
 * @method MarkerQuery groupByOrder() Group by the order column
 * @method MarkerQuery groupByIncluded() Group by the included column
 * @method MarkerQuery groupByIcon() Group by the icon column
 * @method MarkerQuery groupByLat() Group by the lat column
 * @method MarkerQuery groupByLng() Group by the lng column
 * @method MarkerQuery groupByCreateTime() Group by the create_time column
 * @method MarkerQuery groupByRoutesId() Group by the routes_id column
 *
 * @method MarkerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method MarkerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method MarkerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method MarkerQuery leftJoinRoute($relationAlias = null) Adds a LEFT JOIN clause to the query using the Route relation
 * @method MarkerQuery rightJoinRoute($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Route relation
 * @method MarkerQuery innerJoinRoute($relationAlias = null) Adds a INNER JOIN clause to the query using the Route relation
 *
 * @method MarkerQuery leftJoinImage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Image relation
 * @method MarkerQuery rightJoinImage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Image relation
 * @method MarkerQuery innerJoinImage($relationAlias = null) Adds a INNER JOIN clause to the query using the Image relation
 *
 * @method Marker findOne(PropelPDO $con = null) Return the first Marker matching the query
 * @method Marker findOneOrCreate(PropelPDO $con = null) Return the first Marker matching the query, or a new Marker object populated from the query conditions when no match is found
 *
 * @method Marker findOneByTitle(string $title) Return the first Marker filtered by the title column
 * @method Marker findOneByIdMarker(string $id_marker) Return the first Marker filtered by the id_marker column
 * @method Marker findOneByDesciption(string $desciption) Return the first Marker filtered by the desciption column
 * @method Marker findOneByOrder(int $order) Return the first Marker filtered by the order column
 * @method Marker findOneByIncluded(boolean $included) Return the first Marker filtered by the included column
 * @method Marker findOneByIcon(string $icon) Return the first Marker filtered by the icon column
 * @method Marker findOneByLat(double $lat) Return the first Marker filtered by the lat column
 * @method Marker findOneByLng(double $lng) Return the first Marker filtered by the lng column
 * @method Marker findOneByCreateTime(string $create_time) Return the first Marker filtered by the create_time column
 * @method Marker findOneByRoutesId(int $routes_id) Return the first Marker filtered by the routes_id column
 *
 * @method array findById(int $id) Return Marker objects filtered by the id column
 * @method array findByTitle(string $title) Return Marker objects filtered by the title column
 * @method array findByIdMarker(string $id_marker) Return Marker objects filtered by the id_marker column
 * @method array findByDesciption(string $desciption) Return Marker objects filtered by the desciption column
 * @method array findByOrder(int $order) Return Marker objects filtered by the order column
 * @method array findByIncluded(boolean $included) Return Marker objects filtered by the included column
 * @method array findByIcon(string $icon) Return Marker objects filtered by the icon column
 * @method array findByLat(double $lat) Return Marker objects filtered by the lat column
 * @method array findByLng(double $lng) Return Marker objects filtered by the lng column
 * @method array findByCreateTime(string $create_time) Return Marker objects filtered by the create_time column
 * @method array findByRoutesId(int $routes_id) Return Marker objects filtered by the routes_id column
 *
 * @package    propel.generator.maps.om
 */
abstract class BaseMarkerQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseMarkerQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'maps';
        }
        if (null === $modelName) {
            $modelName = 'Marker';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new MarkerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   MarkerQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return MarkerQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof MarkerQuery) {
            return $criteria;
        }
        $query = new MarkerQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Marker|Marker[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MarkerPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(MarkerPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Marker A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Marker A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `title`, `id_marker`, `desciption`, `order`, `included`, `icon`, `lat`, `lng`, `create_time`, `routes_id` FROM `markers` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Marker();
            $obj->hydrate($row);
            MarkerPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Marker|Marker[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Marker[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MarkerPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MarkerPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MarkerPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MarkerPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MarkerPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MarkerPeer::TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the id_marker column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMarker('fooValue');   // WHERE id_marker = 'fooValue'
     * $query->filterByIdMarker('%fooValue%'); // WHERE id_marker LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idMarker The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByIdMarker($idMarker = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idMarker)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idMarker)) {
                $idMarker = str_replace('*', '%', $idMarker);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MarkerPeer::ID_MARKER, $idMarker, $comparison);
    }

    /**
     * Filter the query on the desciption column
     *
     * Example usage:
     * <code>
     * $query->filterByDesciption('fooValue');   // WHERE desciption = 'fooValue'
     * $query->filterByDesciption('%fooValue%'); // WHERE desciption LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desciption The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByDesciption($desciption = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desciption)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desciption)) {
                $desciption = str_replace('*', '%', $desciption);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MarkerPeer::DESCIPTION, $desciption, $comparison);
    }

    /**
     * Filter the query on the order column
     *
     * Example usage:
     * <code>
     * $query->filterByOrder(1234); // WHERE order = 1234
     * $query->filterByOrder(array(12, 34)); // WHERE order IN (12, 34)
     * $query->filterByOrder(array('min' => 12)); // WHERE order >= 12
     * $query->filterByOrder(array('max' => 12)); // WHERE order <= 12
     * </code>
     *
     * @param     mixed $order The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByOrder($order = null, $comparison = null)
    {
        if (is_array($order)) {
            $useMinMax = false;
            if (isset($order['min'])) {
                $this->addUsingAlias(MarkerPeer::ORDER, $order['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($order['max'])) {
                $this->addUsingAlias(MarkerPeer::ORDER, $order['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MarkerPeer::ORDER, $order, $comparison);
    }

    /**
     * Filter the query on the included column
     *
     * Example usage:
     * <code>
     * $query->filterByIncluded(true); // WHERE included = true
     * $query->filterByIncluded('yes'); // WHERE included = true
     * </code>
     *
     * @param     boolean|string $included The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByIncluded($included = null, $comparison = null)
    {
        if (is_string($included)) {
            $included = in_array(strtolower($included), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(MarkerPeer::INCLUDED, $included, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%'); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $icon)) {
                $icon = str_replace('*', '%', $icon);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MarkerPeer::ICON, $icon, $comparison);
    }

    /**
     * Filter the query on the lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE lat >= 12
     * $query->filterByLat(array('max' => 12)); // WHERE lat <= 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(MarkerPeer::LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(MarkerPeer::LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MarkerPeer::LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the lng column
     *
     * Example usage:
     * <code>
     * $query->filterByLng(1234); // WHERE lng = 1234
     * $query->filterByLng(array(12, 34)); // WHERE lng IN (12, 34)
     * $query->filterByLng(array('min' => 12)); // WHERE lng >= 12
     * $query->filterByLng(array('max' => 12)); // WHERE lng <= 12
     * </code>
     *
     * @param     mixed $lng The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByLng($lng = null, $comparison = null)
    {
        if (is_array($lng)) {
            $useMinMax = false;
            if (isset($lng['min'])) {
                $this->addUsingAlias(MarkerPeer::LNG, $lng['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lng['max'])) {
                $this->addUsingAlias(MarkerPeer::LNG, $lng['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MarkerPeer::LNG, $lng, $comparison);
    }

    /**
     * Filter the query on the create_time column
     *
     * Example usage:
     * <code>
     * $query->filterByCreateTime('2011-03-14'); // WHERE create_time = '2011-03-14'
     * $query->filterByCreateTime('now'); // WHERE create_time = '2011-03-14'
     * $query->filterByCreateTime(array('max' => 'yesterday')); // WHERE create_time < '2011-03-13'
     * </code>
     *
     * @param     mixed $createTime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByCreateTime($createTime = null, $comparison = null)
    {
        if (is_array($createTime)) {
            $useMinMax = false;
            if (isset($createTime['min'])) {
                $this->addUsingAlias(MarkerPeer::CREATE_TIME, $createTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createTime['max'])) {
                $this->addUsingAlias(MarkerPeer::CREATE_TIME, $createTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MarkerPeer::CREATE_TIME, $createTime, $comparison);
    }

    /**
     * Filter the query on the routes_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRoutesId(1234); // WHERE routes_id = 1234
     * $query->filterByRoutesId(array(12, 34)); // WHERE routes_id IN (12, 34)
     * $query->filterByRoutesId(array('min' => 12)); // WHERE routes_id >= 12
     * $query->filterByRoutesId(array('max' => 12)); // WHERE routes_id <= 12
     * </code>
     *
     * @see       filterByRoute()
     *
     * @param     mixed $routesId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function filterByRoutesId($routesId = null, $comparison = null)
    {
        if (is_array($routesId)) {
            $useMinMax = false;
            if (isset($routesId['min'])) {
                $this->addUsingAlias(MarkerPeer::ROUTES_ID, $routesId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($routesId['max'])) {
                $this->addUsingAlias(MarkerPeer::ROUTES_ID, $routesId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MarkerPeer::ROUTES_ID, $routesId, $comparison);
    }

    /**
     * Filter the query by a related Route object
     *
     * @param   Route|PropelObjectCollection $route The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MarkerQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRoute($route, $comparison = null)
    {
        if ($route instanceof Route) {
            return $this
                ->addUsingAlias(MarkerPeer::ROUTES_ID, $route->getId(), $comparison);
        } elseif ($route instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MarkerPeer::ROUTES_ID, $route->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRoute() only accepts arguments of type Route or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Route relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function joinRoute($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Route');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Route');
        }

        return $this;
    }

    /**
     * Use the Route relation Route object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RouteQuery A secondary query class using the current class as primary query
     */
    public function useRouteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinRoute($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Route', 'RouteQuery');
    }

    /**
     * Filter the query by a related Image object
     *
     * @param   Image|PropelObjectCollection $image  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 MarkerQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByImage($image, $comparison = null)
    {
        if ($image instanceof Image) {
            return $this
                ->addUsingAlias(MarkerPeer::ID, $image->getMarkerId(), $comparison);
        } elseif ($image instanceof PropelObjectCollection) {
            return $this
                ->useImageQuery()
                ->filterByPrimaryKeys($image->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByImage() only accepts arguments of type Image or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Image relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function joinImage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Image');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Image');
        }

        return $this;
    }

    /**
     * Use the Image relation Image object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ImageQuery A secondary query class using the current class as primary query
     */
    public function useImageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinImage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Image', 'ImageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Marker $marker Object to remove from the list of results
     *
     * @return MarkerQuery The current query, for fluid interface
     */
    public function prune($marker = null)
    {
        if ($marker) {
            $this->addUsingAlias(MarkerPeer::ID, $marker->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
