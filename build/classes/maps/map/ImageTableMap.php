<?php



/**
 * This class defines the structure of the 'images' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.maps.map
 */
class ImageTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'maps.map.ImageTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('images');
        $this->setPhpName('Image');
        $this->setClassname('Image');
        $this->setPackage('maps');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('url', 'Url', 'VARCHAR', true, 1000, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 600, null);
        $this->addColumn('create_time', 'CreateTime', 'DATE', false, null, null);
        $this->addForeignKey('marker_id', 'MarkerId', 'INTEGER', 'markers', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Marker', 'Marker', RelationMap::MANY_TO_ONE, array('marker_id' => 'id', ), null, null);
    } // buildRelations()

} // ImageTableMap
