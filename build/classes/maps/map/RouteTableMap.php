<?php



/**
 * This class defines the structure of the 'routes' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.maps.map
 */
class RouteTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'maps.map.RouteTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('routes');
        $this->setPhpName('Route');
        $this->setClassname('Route');
        $this->setPackage('maps');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 600, null);
        $this->addColumn('short_desciption', 'ShortDesciption', 'VARCHAR', false, 2000, null);
        $this->addColumn('desciption', 'Desciption', 'LONGVARCHAR', false, null, null);
        $this->addColumn('origin', 'Origin', 'VARCHAR', false, 600, null);
        $this->addColumn('image', 'Image', 'VARCHAR', false, 1000, null);
        $this->addColumn('create_time', 'CreateTime', 'DATE', false, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'users', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'User', RelationMap::MANY_TO_ONE, array('user_id' => 'id', ), null, null);
        $this->addRelation('Marker', 'Marker', RelationMap::ONE_TO_MANY, array('id' => 'routes_id', ), null, null, 'Markers');
    } // buildRelations()

} // RouteTableMap
