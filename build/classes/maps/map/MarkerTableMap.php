<?php



/**
 * This class defines the structure of the 'markers' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.maps.map
 */
class MarkerTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'maps.map.MarkerTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('markers');
        $this->setPhpName('Marker');
        $this->setClassname('Marker');
        $this->setPackage('maps');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 1000, null);
        $this->addColumn('id_marker', 'IdMarker', 'VARCHAR', false, 1000, null);
        $this->addColumn('desciption', 'Desciption', 'LONGVARCHAR', false, null, null);
        $this->addColumn('order', 'Order', 'TINYINT', false, null, null);
        $this->addColumn('included', 'Included', 'BOOLEAN', false, 1, null);
        $this->addColumn('icon', 'Icon', 'VARCHAR', false, 255, null);
        $this->addColumn('lat', 'Lat', 'REAL', true, null, null);
        $this->addColumn('lng', 'Lng', 'REAL', true, null, null);
        $this->addColumn('create_time', 'CreateTime', 'DATE', false, null, null);
        $this->addForeignKey('routes_id', 'RoutesId', 'INTEGER', 'routes', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Route', 'Route', RelationMap::MANY_TO_ONE, array('routes_id' => 'id', ), null, null);
        $this->addRelation('Image', 'Image', RelationMap::ONE_TO_MANY, array('id' => 'marker_id', ), null, null, 'Images');
    } // buildRelations()

} // MarkerTableMap
