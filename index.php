<?php
session_start();
include_once './build/config.php';


if (isset($_GET['close'])) {
    session_unset();
    session_destroy();
}
?>
<html>
    <head>
        <title>Routes</title>
        <?php include_once './components/templates/header.php'; ?>
        <script src="<?php echo $config['paths']['loginDir'] ?>login.js"></script>

    </head>
    <body>
        <div id="head">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php include_once $config['pathsAbs']['templateDir'] . "head.php"; ?>   
                    </div>
                </div>
            </div>
        </div>
        <div id="body" >
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        if (isset($_SESSION['user'])) {
                            echo "Ya hay una sesión iniciada";
                        } else {
                            include_once $config['pathsAbs']['loginDir'] . "login.html";
                        }
                        ?>  

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 block">
                        <div>
                            <h1 style=""><span class="glyphicon glyphicon-map-marker"></span></h1>
                            <h3 class="page-header">Crea tu ruta, marca los lugares por donde vas a pasar </h3>
                        </div>
                    </div>
                    <div class="col-sm-4 block">
                        <div>
                            <h1 style=""><span class="glyphicon glyphicon-pushpin"></span></h1>
                            <h3 class="page-header">Introduce información, añade detalles e imágenes </h3>
                        </div>
                    </div>
                    <div class="col-sm-4 block">
                        <div>
                            <h1 style=""><span class="glyphicon glyphicon-print"></span></h1>
                            <h3 class="page-header">Imprímela y sal a la calle a recorrerla </h3>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
        <div id="foot">
            <div class="container">
                <div class="row">
                    <div id="foot" class="col-lg-12">
                        <?php include_once $config['pathsAbs']['templateDir'] . "footer.php"; ?>   
                    </div>
                </div>
            </div>
        </div>


    </body>
</html>



