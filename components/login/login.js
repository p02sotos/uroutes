/* 
 * Creado por Sergio Soriano Toro contacto@sergiosoriano.com
 */
$(document).ready(function() {
    //CHANGE THIS VARIABLES TO YOURS
    var validateFormId = "#form-login";
    var validateFormCreateId = "#form-createUser";
    var loginBtnId = "#submitBtn";
    var createBtnId = "#createBtn";
    var userId = "#inputUser";
    var passId = "#inputPassword";

    //=============================================================



    $(validateFormId).validate({
        rules: {
            inputUser: {
                required: true,
                minlength: 5
            },
            inputPassword: {
                required: true,
                minlength: 4
            },
        },
        submitHandler: function(form) {
            //Preparamos los datos para pasarlos
            ajaxSubLogin(userId, passId, loginBtnId);
        },
        messages: {
            inputUser: {
                required: "Por favor, especifique el usuario",
                minlength: "Mínimo 5 "
            },
            inputPassword: {
                required: "Por favor, introduzca una contraseña",
                minlength: "Mínimo 5 "
            },
            errorClass: "alert alert-danger",
            errorElement: "div"

        }
    });

    $(validateFormCreateId).validate({
        rules: {
            inputUser: {
                required: true,
                minlength: 5
            },
            inputPassword: {
                required: true,
                minlength: 4
            },
            repeatPass: {
                required: true,
                equalTo: "#inputPassword",
                minlength: 4
            }

        },
        submitHandler: function(form) {
            //Preparamos los datos para pasarlos
            ajaxSubLogin(userId, passId, createBtnId);
        },
        messages: {
             inputUser: {
                required: "Por favor, especifique el usuario",
                minlength: "Mínimo 5 "
            },
            inputPassword: {
                required: "Por favor, introduzca una contraseña",
                minlength: "Mínimo 5 "
            },
            repeatPass: {
                required: "Por favor introduzca un password",
                equalTo: "Compruebe si ha introducido el mismo password que arriba"
            }
        },
        errorClass: "alert alert-danger",
        errorElement: "div"
    });


    var ajaxSubLogin = function(user, pass, action) {
        var u = $(user).val();
        var p = $(pass).val();
        var a = $(action).val();
        var url;
        if (a == "createUser") {
            url = "../../build/controller.php";
        } else {
            url = 'build/controller.php';
        }
        var data = new FormData();
        data.append('user', u);
        data.append('pass', p);
        data.append('action', a);
        $.ajax({
            url: url,
            data: data,
            processData: false,
            contentType: false,
            type: 'POST',
            
            success: function(data) { 
                
                if(data=='Password Error'){
                    alert(data);
                }
                else if (data=='Usuario Desconocido'){
                    alert(data);
                }
               else {
                   window.location.replace("components/maps/desktop.php");
               }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
    $(loginBtnId).click(function(event) {
        $(validateFormId).submit();
    });
    $(createBtnId).click(function(event) {
        $(validateFormCreateId).submit();
    });
});


