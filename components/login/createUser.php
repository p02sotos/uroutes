<?php
include_once '../../build/config.php';
include_once  $config['pathsAbs']['loginDir'].'security.php';
?>
<html>
    <head>
        <title>Routes</title>
        <?php include_once '../../components/templates/header.php'; ?>
        <script src="<?php echo $config['paths']['loginDir'] ?>/login.js"></script>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div id="head" class="col-sm-12">
                    <?php include_once '../../components/templates/head.php'; ?>   
                </div>
            </div>
            <div class="row">
                <div id="body" class="col-sm-12">
                    <div id="createUser">
                        <form name="form-createUser" id="form-createUser" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="inputUser" class="col-sm-2 control-label">Usuario</label>
                                <div class="col-sm-3">
                                    <input name="inputUser" type="text" class="form-control" id="inputUser" placeholder="Enter User">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-2 control-label">Enter Password</label>
                                <div class="col-sm-3">
                                    <input name="inputPassword" type="password" class="form-control" id="inputPassword" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="repeatPass" class="col-sm-2 control-label">Repear Password</label>
                                <div class="col-sm-3">
                                    <input name="repeatPass" type="password" class="form-control" id="repeatPass" placeholder="Repeat Password">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-3">
                                    <button id="createBtn" type="button" class="btn btn-default" value="createUser">Entrar</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <div class="row">
                <div id="foot" class="col-lg-12">
                    <?php include_once '../../components/templates/footer.php'; ?>   
                </div>
            </div>
        </div>


    </body>
</html>

