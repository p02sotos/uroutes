<?php
include_once $_SERVER['DOCUMENT_ROOT'] ."/" . "Routes/build/" . 'config.php';
?>

<html>
    <head>
        <title>Error</title>
        <?php include_once $config['pathsAbs']['templateDir']."header.php" ?>     
        
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div id="head" class="col-sm-12">
                    <?php include_once '../templates/head.php'; ?>   
                </div>
            </div>
            <div class="row">
                <div id="body" class="col-sm-12">
                    <div class="alert alert-warning">
                        <p>Lo sentimos, no tiene los permisos necesarios para estar en esta página. Por favor
                            autentifiquese. </p>
                        <p><a href="../../index.php" class="btn btn-primary">Volver...</a></p>
                    
                    </div>
                  
                </div>
            </div>
            <div class="row">
                <div id="foot" class="col-lg-12">
                    <?php include_once '../templates/footer.php'; ?>   
                </div>
            </div>
        </div>


    </body>
</html>

