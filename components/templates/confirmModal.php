<div id="confirmReset" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3>Confirmar Borrar Ruta</h3>
            </div>
            <div class="modal-body">
                ¿Está seguro que desea borrar los puntos marcados?
            </div>
            <div class="modal-footer">
                <button id="btnBorrar" class="btn btn-warning">Borrar</button>
                <button  id="btnCancel" class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            </div>
        </div>
    </div>
</div>