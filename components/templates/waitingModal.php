<div id="waitingModal" class="modal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >  
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">                
                <h4 id="myModalLabel" class="modal-title">Espere por favor, Guardando datos</h4>
            </div>
            <div class="modal-content">

                <div id="waiting" class="center-block"></div>
            </div>
        </div>
    </div>
</div>