<div id="editMarker" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="nameMarker"> </h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">                   
                   
                     <!--<div class="form-group">
                        <label for="shortDes" class="col-sm-3 control-label">DESCRIPCIÓN CORTA</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="shortDes" placeholder="Descripción rápida">
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label for="des" class="col-sm-2 control-label">DESCRIPCIÓN</label>
                        <div class="col-sm-10">
                            <textarea  class="form-control" id="des" rows="10" placeholder="Descripción"></textarea>
                        </div>
                    </div>                                    
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnSave" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Guardar</button>
                <button  id="btnCancel" class="btn  btn-default" data-dismiss="modal" aria-hidden="true">Cancelar</button>
            </div>
        </div>
    </div>
</div>