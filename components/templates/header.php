<?php

?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<link rel = 'stylesheet' href = "<?php echo $config['paths']['componentsDir'] ?>bootstrap-3.1.1-dist/css/bootstrap.min.css" type = 'text/css' />
<link rel = "stylesheet" href = "<?php echo $config['paths']['mapsDir'] ?>css/style.css" type = "text/css" />
<script src="<?php echo $config['paths']['componentsDir'] ?>jquery/jquery.min.js"></script>
<script src="<?php echo $config['paths']['componentsDir']?>jquery/validator/jquery.validate.min.js"></script>
<script src="<?php echo $config['paths']['componentsDir'] ?>bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>

