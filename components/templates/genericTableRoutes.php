<?php

if (isset($dataArray)) {
    $actualId;
    echo '<div class="center-block">';
    echo "<button name='new' value='new' id='newRoute' class='btn btn-danger' ><span class='glyphicon glyphicon-plus'> </span> Nueva Ruta </button>";
    echo '</div>';
    echo '<div id="freewall" class="free-wall">';
    foreach ($dataArray as $key => $value) {
        foreach ($value as $key2 => $value2) {
            if ($key2 == 'Id') {
                //Guardo el valor pero no pinto
                $actualId = $value2;
            } else if ($key2 == 'Name') {
                $name = $value2;
            } else if ($key2 == 'UserId') {
                //No pinto la casilla
            } else if ($key2 == 'Desciption') {
                $description = $value2;
            } else if ($key2 == 'Image') {
                $image = $value2;
            }
            else if ($key2 == 'Origin') {
                $origin = $value2;
            }
        }
        echo '<div class="brick">';
        echo '<h3>' . $name . '</h3>';
        echo '<hr/>';
        echo "<img src='$image' width='100%'>";
        echo '<div class="fo">';


        //echo '<h5>' . $description . '</h5>';
        echo '<div class="actionBtn" >';
        echo '<div class="btn-group">';
        echo "<button name='delete' value='$actualId' class='deleteClass btn btn-danger' rel='tooltip'  data-toggle='tooltip'  data-placement='top' title='Borrar Ruta'><span class='glyphicon glyphicon-trash'> </span></button>"
        . "<button name='edit' value='$actualId' class='btn btn-default' rel='tooltip' data-toggle='tooltip' data-placement='top' title='Editar Ruta'><span class='glyphicon glyphicon-edit'> </span></button>"
        . "<a  href='map.php?action=editRoute&id=$actualId&origin=$origin' rel='tooltip' data-toggle='tooltip' data-placement='top' title='Editar Marcadores de Ruta en el Mapa' class='btn btn-default'><span class='glyphicon glyphicon-map-marker'</span> </a>"
        . "<a  href='print.php?action=print&routeId=$actualId' rel='tooltip' data-toggle='tooltip' data-placement='top' title='Imprime la ruta' class='btn btn-default'><span class='glyphicon glyphicon-print'</span> </a>";
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
    }
    echo '</div>';
} else {
    echo "No hay resultados";
    echo "<button name='new' value='new' id='newRoute' class='btn btn-danger' ><span class='glyphicon glyphicon-plus'> </span> Nueva Ruta </button>";
}
?>







