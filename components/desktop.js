/* 
 * Creado por Sergio Soriano Toro contacto@sergiosoriano.com
 */

$(document).on('ready', function() {
    var id;
    var action;
    var description;
    var name;
    var short;
    var origin;
    var image;
    var opt = {
        lines: 11, // The number of lines to draw
        length: 40, // The length of each line
        width: 10, // The line thickness
        radius: 50, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 58, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1.3, // Rounds per second
        trail: 76, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%' // Top position relative to parent in px
                // Left position relative to parent in px
    };
    $("button[name='new']").on('click', function(event) {
        action = 'newRoute';
        $('#description').editable({
            inlineMode: false,
            zIndex: 2001,
            height: 400,
            imageUploadURL: 'uploadImage.php',
            // CORS. Only if needed.            
            // Additional upload params.
            imageDeleteURL: 'deleteImage.php',
            imageButtons: ["floatImageLeft", "floatImageNone", "floatImageRight", "removeImage"],
            afterRemoveImageCallback: function(src) {
                deleteImage(src);
            }
            // Set the image error callback.
        });
        $('#nameRoute').editable({
            inlineMode: true,
            zIndex: 2001,
            buttons: ["bold", "italic", "underline", "color"],
            placeholder: "Título"
        });
        $('#shortDes').editable({
            inlineMode: false,
            zIndex: 2001,
            height: 200,
            buttons: ["bold", "italic", "underline", "color", "insertImage"],
            // Set the image upload parameter.

            // Set the image upload URL.
            imageUploadURL: 'uploadImage.php',
            // CORS. Only if needed.            
            // Additional upload params.
            imageDeleteURL: 'deleteImage.php',
            imageButtons: ["floatImageLeft", "floatImageNone", "floatImageRight", "removeImage"],
            // Set the image error callback.
            afterRemoveImageCallback: function(src) {
                deleteImage(src);
            },
            insertImageCallback: function(imageURL) {
                image = imageURL;
            },
            imageErrorCallback: function(data) {
                // Bad link.
                if (data.errorCode === 1) {
                }
                // No link in upload response.
                else if (data.errorCode === 2) {
                }
                // Error during file upload.
                else if (data.errorCode === 3) {
                }
                // Parsing response failed.
                else if (data.errorCode === 4) {
                }
            }
        });
        $('#nameRoute').editable("setHTML", '', false);
        $('#city').val("");
        $('#shortDes').editable("setHTML", '', false);
        $('#nameRoute').editable("setHTML", '', false);
        $('#description').editable("setHTML", '', false);
        $("#editRoute").modal({
            backdrop: 'static',
            show: true
        });
    });
    $("button[name='edit']").on('click', function(event) {
        id = $(this).val();
        action = 'editRoute';
        console.log(datas);
        for (var i in datas) {
            if (datas[i].Id == id) {
                description = datas[i].Desciption;
                name = datas[i].Name;
                console.log(datas[i].Image);
                image = datas[i].Image;
                origin = datas[i].Origin;
                short = datas[i].ShortDesciption;
                console.log(name + description + image);
            }
        }
        $('#description').editable({
            inlineMode: false,
            zIndex: 2001,
            height: 400,
            imageUploadURL: 'uploadImage.php',
            // CORS. Only if needed.            
            // Additional upload params.
            imageDeleteURL: 'deleteImage.php',
            afterRemoveImageCallback: function(src) {
               deleteImage(src);
            },
            imageButtons: ["floatImageLeft", "floatImageNone", "floatImageRight", "removeImage"],
            // Set the image error callback.
        });
        $('#nameRoute').editable({
            inlineMode: true,
            zIndex: 2001,
            buttons: ["bold", "italic", "underline", "color"],
            placeholder: "Título"
        });
        $('#shortDes').editable({
            inlineMode: false,
            zIndex: 2001,
            height: 200,
            buttons: ["bold", "italic", "underline", "color", "insertImage"],
            // Set the image upload URL.
            imageUploadURL: 'uploadImage.php',
            // CORS. Only if needed.            
            // Additional upload params.
            imageDeleteURL: 'deleteImage.php',
            afterRemoveImageCallback: function(src) {
                deleteImage(src);
            },
            imageButtons: ["floatImageLeft", "floatImageNone", "floatImageRight", "removeImage"],
            // Set the image error callback.
            insertImageCallback: function(imageURL) {
                image = imageURL;
                console.log("Despues de añadir" + image);
            },
            imageDeleteSuccessCallback: function(data) {
                alert(data);
            },
            imageErrorCallback: function(data) {
                // Bad link.
                console.log(data);
                alert(data);
                if (data.errorCode === 1) {
                }
                // No link in upload response.
                else if (data.errorCode === 2) {
                }
                // Error during file upload.
                else if (data.errorCode === 3) {
                }
                // Parsing response failed.
                else if (data.errorCode === 4) {
                }
            }

        });
        $('#nameRoute').editable("setHTML", name, false);
        $('#shortDes').editable("setHTML", short, false);
        $('#city').val(origin);
        $('#description').editable("setHTML", description, false);
        $("#editRoute").modal({
            backdrop: 'static',
            show: true
        });
    });
    $("#btnSave").click(function() {
//CARGAR TODOS LO DATOS DEL FORMULARIO Y PASARLOS AL AJAX
        name = $('#nameRoute').editable('getHTML'); //Devuelve un Array
        console.log(name);
        description = $('#description').editable('getHTML');
        console.log(description);
        origin = $('#city').val();
        console.log(origin);
        console.log(image);
        short = $('#shortDes').editable('getHTML');
        console.log(short);
        alert('parar');
        $.ajax({
            url: '../../build/controller.php',
            data: {
                id: id,
                action: action,
                name: name,
                description: description,
                image: image,
                origin: origin,
                short: short
            },
            type: 'POST',
            success: function(data) {

                $('#waitingModal').modal('hide');
                location.reload(true);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $('#waitingModal').modal('hide');
                alert(thrownError);
            }
        });
    });
    $("button[name='delete']").on('click', function(event) {
        id = $(this).val();
        $("#confirmReset").modal('show');
    });
    var target = document.getElementById('waiting');
    var spinner = new Spinner(opt).spin(target);
    $(".spinner:first-child").addClass('center-block');
    $(".brick .actionBtn").addClass('center-block');
    $(function() {
        $('[rel="tooltip"]').tooltip();
    });
    var wall = new freewall("#freewall");
    wall.reset({
        selector: '.brick',
        animate: true,
        cellW: 300,
        cellH: 'auto',
        onResize: function() {
            wall.refresh();
        }
    });
    wall.fitWidth();
    // for scroll bar appear;
    $(window).trigger("resize");
    var id;
    $("#btnBorrar").click(function() {

        $("#confirmReset").show();
        var action = "deleteRoute";
        $("#confirmReset").modal('hide');
        $('#waitingModal').modal({
            show: true,
            keyboard: false,
            backdrop: 'static'

        });
        $.ajax({
            url: '../../build/controller.php',
            data: {
                id: id,
                action: action
            },
            type: 'POST',
            success: function(data) {
                $('#waitingModal').modal('hide');
                location.reload(true);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $('#waitingModal').modal('hide');
                alert(thrownError);
            }
        });
    });
    $("#btnCancel").click(function() {
        $("#confirmReset").modal('hide');
    });
    //$("#btn").modal('show');

    function deleteImage(src){
        $.ajax({
            url: 'deleteImage.php',
            data: {
                src: src               
            },
            type: 'POST',
            success: function(data) {
                alert("deleted");
            },
            error: function(xhr, ajaxOptions, thrownError) {
                
                alert(thrownError);
            }
        });
        
    }


});


