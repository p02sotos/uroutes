<?php
/*
 * Creado por Sergio Soriano Toro contacto@sergiosoriano.com
 */

include_once $_SERVER['DOCUMENT_ROOT'] . "/" . "Routes" . "/build/config.php";
include_once $config['pathsAbs']['loginDir'] . 'security.php';
include_once $config['pathsAbs']['controllerPage'];
require_once $config['pathsAbs']['vendorDir'] . '/propel/runtime/lib/Propel.php';
require_once $config['pathsAbs']['componentsDir'] . 'FirePHPCore/FirePHP.class.php';
Propel::init($config['pathsAbs']['buildDir'] . "conf/maps-conf.php");
set_include_path($config['pathsAbs']['buildDir'] . "classes" . PATH_SEPARATOR . get_include_path());
ob_start();
$firephp = FirePHP::getInstance(true);
if (isset($_GET['routeId'])) {
    $id = $_GET['routeId'];
}

$route = RouteQuery::create()->findPk($id);
$markers = $route->getMarkers();
$firephp->log($markers, "Marcadores");
$marker = new Marker();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Imprimir Ruta</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <?php include_once $config['pathsAbs']['templateDir'] . "header.php"; ?>
        <link href="css/print.css" type="text/css" rel="stylesheet">
        <style type="text/css">
            html { height: 100% }
            body { height: 100%; margin: 0; padding: 0 }
        </style>  
    </head>
    <body>


        <!-- CUERPO -->
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div> 
                            <h1 class="titleRoute"><?php echo $route->getName(); ?></h1>
                            <h3 class="origin"><?php echo "Origen: ".$route->getOrigin(); ?></h3>
                            <p class="lead shortDesRoute"><?php echo $route->getShortDesciption(); ?></p>
                            <blockquote class="desRoute"><p><?php echo $route->getDesciption(); ?></p></blockquote>
                            <hr/>
                            <h2 style="text-align: center">Puntos de Ruta</h2>
                            <hr/>
                            <?php
                            foreach ($markers as $key => $value) {
                                $firephp->log($value);
                                if ($value->getIncluded()) {
                                    $title = $value->getTitle();
                                    $des = $value->getDesciption();
                                    echo "<h3 class='titleMarker'>$title</h3>";
                                    echo "<hr/>";
                                    echo "<blockquote class='desMarker'><p>$des</p></blockquote>";
                                }
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>



    </body>
</html>


