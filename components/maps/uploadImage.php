<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/" . "Routes" . "/build/config.php";
include_once $config['pathsAbs']['loginDir'] . 'security.php';
require_once $config['pathsAbs']['componentsDir'] . 'FirePHPCore/FirePHP.class.php';
ob_start();
$firephp = FirePHP::getInstance(true);
/* 
 * Creado por Sergio Soriano Toro contacto@sergiosoriano.com
 */

// Allowed extentions.
$allowedExts = array("gif", "jpeg", "jpg", "png");
$firephp->log($allowedExts, 'Extension');
// Get filename.
$firephp->log($_FILES, 'FILE');
$temp = explode(".", $_FILES["file"]["name"]);
$firephp->log($temp, 'File');
// Get extension.
$extension = end($temp);
$firephp->log($extension, 'extension');
// An image check is being done in the editor but it is best to
// check that again on the server side.
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/jpg")
|| ($_FILES["file"]["type"] == "image/pjpeg")
|| ($_FILES["file"]["type"] == "image/x-png")
|| ($_FILES["file"]["type"] == "image/png"))
&& in_array($extension, $allowedExts)) {
    // Generate new random name.
    $name = "User-".$_SESSION['userId']."-".$temp[0].".".$extension;

    // Save file in the uploads folder.
    move_uploaded_file($_FILES["file"]["tmp_name"], "img/users/" . $name);

    // Generate response.
    
    $response = new StdClass;
    $response->link = "img/users/" . $name;
    echo stripslashes(json_encode($response));
}