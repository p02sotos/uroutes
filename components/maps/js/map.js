$(document).ready(function() {
    //Configuración inicial
    var mapCenter = new google.maps.LatLng(37.891586, -4.7844853); //Córdoba
    var map;
    var poly;
    var markers = [];
    var Marcadores = [];
    var path;
    var infoWindows = [];
    var edit = false;
    var selectedMarker;
    var geocoder;
    var marcadorIdtoEdit;
    var marcador;
    var polyOptions = {
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        editable: false,
        draggable: false
    };
    //Google map option
    var googleMapOptions =
            {
                center: mapCenter, // map center
                zoom: 15, //zoom level, 0 = earth view to higher value
                panControl: true, //enable pan Control
                zoomControl: true, //enable zoom control
                disableDefaultUI: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL //zoom control size
                },
                scaleControl: true, // enable scale control
                mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
            };



    map_initialize();
    function map_initialize() {
        //Creamos el geolocalizador
        geocoder = new google.maps.Geocoder();
        //Creamos el mapa
        var mapDiv = document.getElementById("google_map");
        map = new google.maps.Map(mapDiv, googleMapOptions);
        //Crear Botones del Mapa
        createButtons(map);
        //Acción al pulsar en el mapa
        google.maps.event.addListener(map, 'click', function(event) {
            addMarket(map, event.latLng, null, null, null, new Date(), null, markers.length);
        });
        //POLILINEA
        createPoly(map);
        google.maps.event.addListener(poly, 'rightclick', function(event) {
            path.pop();
            var marker = markers.pop();
            marker.setMap(null);
        });
        //Obtenemos datos de la base de datos
        var externalMarkers = getData();
        console.log("Marcadores cargados" + externalMarkers);
        if (externalMarkers !== 'new') {
            edit = true;
            createMarkers(externalMarkers);
        } else {
            markers = [];
            Marcadores = [];
            path = [];
            edit = false;
            createPoly(map);
            var externalMarkers = getData();
        }

        /*
         * Añade un marcador y asocia todas las funciones a él.
         * @param {google.map.Map} map
         * @param {google.maps.LatLng} latLng
         * @returns {undefined}
         */
        function addMarket(map, latLng, title, icon, description, createTime, included, order) {
            //Comprobamos si es un marcador nuevo para poner reiniciar variables

            if ((typeof description === "undefined") || (description === null)) {
                description = "";
            }
            if ((typeof title === "undefined") || (title === null)) {
                title = "";
            }
            if ((typeof createTime === "undefined") || (createTime === null)) {
                createTime = new Date();
            }
            if ((typeof included === "undefined") || (included === null)) {
                included = 0;
            }

            var marker = new google.maps.Marker({
                position: latLng, //map Coordinates where user right clicked
                map: map,
                draggable: false, //set marker draggable 
                animation: google.maps.Animation.DROP, //bounce animation
                title: "marcador-" + markers.length,
                icon: icon
                        //custom pin icon
            });
            path = poly.getPath();
            path.push(latLng);
            markers.push(marker);
            includeMarcador(marker, title, description, included, createTime, order);
            //Marcadores.push();
            var textBtn = "Incluir";
            var textDisabled = "disabled = true";
            if (icon !== null && icon === "img/marker.png") {
                textBtn = "Incluir";
                textDisabled = "disabled = true";

            } else if (icon !== null && icon === "img/pin_blue.png") {
                textBtn = "Excluir";
                textDisabled = "";
            }
            var contentString = $(
                    '<div id="' + marker.getTitle() + '" class="marker-info-win">' +
                    '<div class="marker-inner-win"><span class="info-content">' +
                    '<h1 class="marker-heading">Punto de Ruta</h1>' +
                    '' +
                    '¿Añadir parada a la ruta?' +
                    '</span>' +
                    '<br />' +
                    '<div class = "btn-group" >' +
                    '<!--<button name="remove-marker" class="remove-marker btn btn-default" title="Remove Marker to Route">Borrar</button>-->' +
                    '<button name="add-marker" value="' + marker.getTitle() + '" class="add-marker btn btn-default" title="Add Marker to Route">' + textBtn + '</button>' +
                    '<button id="edit-marker"' + textDisabled + ' value="' + marker.getTitle() + '" name="edit-marker" class="edit-marker btn btn-primary" title="Editar más opciones marcador">Detalles</button>' +
                    '</div>' +
                    '</div></div>'
                    );
            var infowindow = new google.maps.InfoWindow();
            infowindow.setContent(contentString[0]);
            //closeOtherAndOpenInfoWindow(infowindow, marker);
            google.maps.event.addListener(marker, 'click', function() {
                closeOtherAndOpenInfoWindow(infowindow, marker);
                selectedMarker = marker;
            });
            if (edit) {
                $('#title').attr('value', title);
                $('#descripcion').html(description);
            }
            var addBtn = contentString.find('button.add-marker')[0];
            google.maps.event.addDomListener(addBtn, "click", function() {
                var value = $(this).val();
                marcadorIdtoEdit = arrayObjectIndexOf(Marcadores, value, 'id');

                console.log("ID, Marcador: " + value);
                console.log("ID Marcador extraido: " + marcadorIdtoEdit);
                marcador = Marcadores[marcadorIdtoEdit];
                console.log("Marcadores : " + JSON.stringify(Marcadores));
                console.log("Marcador extraido: " + JSON.stringify(marcador));

                if ($('.add-marker').html() === 'Excluir') {
                    marker.setIcon("img/marker.png");
                    includeMarcador(marker, marcador.title, marcador.description, 0, marcador.createDate, marcador.order);
                    $('.add-marker').html('Incluir');
                    $('.edit-marker').prop("disabled", true);
                } else {

                    $('.add-marker').html('Excluir');
                    $('.edit-marker').prop("disabled", false);
                    if (marker.getIcon() !== "img/pin_blue.png") {
                        marker.setIcon("img/pin_blue.png");
                        includeMarcador(marker, marcador.title, marcador.description, 1, marcador.createDate, marcador.order);
                    }
                }


            });
            var editBtn = contentString.find('button.edit-marker')[0];
            google.maps.event.addDomListener(editBtn, "click", function() {
                $('#des').editable({
                    inlineMode: false,
                    zIndex: 2001,
                    height: 300
                });
                $('#nameMarker').editable({
                    inlineMode: true,
                    zIndex: 2001,
                    buttons: ["bold", "italic", "underline", "color"],
                    //placeholder: "Título del punto de parada"
                });
                $("#editMarker").modal({
                    backdrop: 'static',
                    show: true
                });
                var value = $(this).val();
                marcadorIdtoEdit = arrayObjectIndexOf(Marcadores, value, 'id');
                marcador = Marcadores[marcadorIdtoEdit];
                $('#nameMarker').editable("setHTML", marcador.title, false);
                $('#des').editable("setHTML", marcador.description, false);
                var saveBtn = $("button#btnSave")[0];
                google.maps.event.addDomListener(saveBtn, "click", function() {
                    var title = $('#nameMarker').editable('getHTML'); //Devuelve un Array
                    console.log("Titulo a guardar" + title);
                    var des = $('#des').editable('getHTML'); //Devuelve un Array
                    console.log("Descripción a guardar: " + des);
                    marcador.title = title[0];
                    marcador.description = des[0];
                    marcador.included = 1;
                    Marcadores[marcadorIdtoEdit] = marcador;
                    //includeMarcador(marker, title[0], des[0], 1, marcador.createDate, marcador.order);

                });
            });

        }
        ;

        /**
         * Función que colocal los marcadores a partir de un array de marcadores dado de la base de datos
         * @param {type} markers
         * @returns {undefined}
         */
        function createMarkers(markers) {
            var markersJSON = eval('(' + markers + ')');
            var origin = getOrigin();
            codeAddress(origin)
            console.log("Origen: " + origin);
            //console.log('Marcadores a mostrar: ' + markersJSON);
            for (var pro in markersJSON) {
                var createTime = markersJSON[pro]['CreateTime'];
                var description = markersJSON[pro]['Desciption'];
                var icon = markersJSON[pro]['Icon'];
                var lat = markersJSON[pro]['Lat'];
                var lng = markersJSON[pro]['Lng'];
                var included = markersJSON[pro]['Included'];
                var order = markersJSON[pro]['Order'];
                var title = markersJSON[pro]['Title'];
                var pos = new google.maps.LatLng(lat, lng, true);
                addMarket(map, pos, title, icon, description, createTime, included, order);
            }
        }
        /*
         * Función que limpia la ruta y lo que haya guardado en ella
         * @returns {_L1.reiniciarTodo}
         */
        function reiniciarTodo() {
            $.each(markers, function(index, marker) {
                marker.setMap(null);
            });
            markers = [];
            Marcadores = [];
            path.clear();
        }
        /**
         * Función que busca y centra el mapa de una dirección dada
         * @param {type} address
         * @returns {undefined}
         */
        function codeAddress(address) {
            geocoder.geocode({'address': address}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
        /*
         * Función que añade el marcador añadido a un Array que puede ser mandado por Ajax. 
         * Para guardar los datos en la base de datos.
         * @param {google.maps.Marker} marker
         * @param {String} name
         * @param {String} description
         * @returns {undefined}
         */
        function includeMarcador(marker, title, description, included, createDate, order) {
            console.log("Date: " + createDate);
            var latLng = marker.getPosition();
            var id = marker.getTitle();
            var mark = new Marcador(id, included, title, description, marker.getIcon(), latLng.lat(), latLng.lng(), order, createDate);
            if (arrayObjectIndexOf(Marcadores, id, "id") !== -1) {
                Marcadores[arrayObjectIndexOf(Marcadores, id, "id")] = mark;
            } else {
                Marcadores.push(mark);
            }
        }



        /*
         * Función para mandar los datos de la ruta guardada en AJAX al servidor y guardarlo en la base de 
         * datos
         * @param {Array de Markers} markers
         * @returns {undefined}
         */

        function saveMarkers(marcadores) {
//Hacer que sobreescriba la ruta actual o que salve una nueva segun sea el caso
            console.log("Id de la ruta mandado: " + id);
            var action = "storeMarkers";

            $.ajax({
                url: '../../build/controller.php',
                data: {
                    marcadores: marcadores,
                    routeId: id,
                    action: action
                },
                type: 'POST',
                success: function(data) {
                   
                },
                error: function(xhr, ajaxOptions, thrownError) {
                   
                }
            });
        }

        /**
         * FUNCION PARA CERRAR TODAS LAS INFOWINDOWS Y ABRIR LA PULSADA
         * @param {type} infowindow
         * @param {type} marker
         * @returns {undefined}
         */
        function closeOtherAndOpenInfoWindow(infowindow, marker) {
            infowindow.open(map, marker);
            if (infoWindows.length !== 0) {
                for (var i = 0; i < infoWindows.length; i++) {
                    if (infoWindows[i] !== infowindow) {
                        infoWindows[i].close();
                    }
                }
            }
            infoWindows.push(infowindow);
        }
        ;

        function ControlsMap(controlDiv, map) {
            controlDiv.style.padding = '5px';
            controlDiv.setAttribute('class', 'btn-group');
            //Creamos un botón
            var btnSave = document.createElement('button');
            btnSave.setAttribute('class', 'btn btn-default');
            btnSave.textContent = "Guardar";
            btnSave.setAttribute('id', 'saveBtn');
            controlDiv.appendChild(btnSave);
            var btnReset = document.createElement('button');
            btnReset.setAttribute('class', 'btn btn-danger');
            btnReset.textContent = "Reiniciar";
            btnReset.setAttribute('id', 'resetBtn');
            controlDiv.appendChild(btnReset);
            var btnCancel = document.createElement('button');
            btnCancel.setAttribute('class', 'btn btn-warning');
            btnCancel.textContent = "Cancelar";
            btnCancel.setAttribute('id', 'cancelBtn');
            controlDiv.appendChild(btnCancel);
            //Aquí ponemos los eventos de los botones
            google.maps.event.addDomListener(btnSave, 'click', function() {

                saveMarkers(Marcadores);
                window.location = "desktop.php";
            });
            google.maps.event.addDomListener(btnReset, 'click', function() {

                reiniciarTodo();
            });
            google.maps.event.addDomListener(btnCancel, 'click', function() {

                window.location = "desktop.php";
            });


        }
        function SearchBox(controlDiv, map) {
            controlDiv.style.padding = '5px';
            controlDiv.setAttribute('class', 'col-sm-8');

            var searchBox = document.createElement('form');
            searchBox.setAttribute('class', '');
            searchBox.setAttribute('role', 'search');

            var div1 = document.createElement('div');
            div1.setAttribute('class', 'form-group  col-sm-4');
            var inputSearch = document.createElement('input');
            inputSearch.setAttribute('id', 'adress');
            inputSearch.setAttribute('type', 'text');
            inputSearch.setAttribute('value', 'Córdoba');
            inputSearch.setAttribute('class', 'form-control');
            div1.appendChild(inputSearch);
            searchBox.appendChild(div1);
            var btnSearch = document.createElement('button');
            btnSearch.setAttribute('id', 'btnSearch');
            btnSearch.setAttribute('type', 'button');
            btnSearch.setAttribute('value', 'search');
            btnSearch.setAttribute('class', 'btn btn-primary');
            btnSearch.textContent = 'Centrar';

            controlDiv.appendChild(searchBox);
            controlDiv.appendChild(btnSearch);
            google.maps.event.addDomListener(btnSearch, 'click', function() {
                
                var address = document.getElementById('adress').value;
                codeAddress(address);
            });
        }
        function createButtons(map) {
            var controlBtns = document.createElement('div');
            var seachFormDiv = document.createElement('div');
            var homeControl = new ControlsMap(controlBtns, map);
            var searchForm = new SearchBox(seachFormDiv, map);
            controlBtns.index = 1;
            seachFormDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlBtns);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(seachFormDiv);
        }
        function createPoly(map) {
            poly = new google.maps.Polyline(polyOptions);
            poly.setMap(map);
            path = poly.getPath();

        }

        /*
         * 
         * Función que busca si existe un elemento en un array comparando por un elemento del Array
         */
        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for (var i = 0, len = myArray.length; i < len; i++) {
                if (myArray[i][property] == searchTerm)
                    return i;
            }
            return -1;
        }
        function Marcador(id, included, title, description, icon, lat, lng, order, createDate) {
            this.id = id;
            this.included = included;
            this.title = title;
            this.description = description;
            this.icon = icon;
            this.lat = lat;
            this.lng = lng;
            this.order = order;
            this.createDate = createDate;
        }
    }
});







