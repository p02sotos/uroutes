$(document).ready(function() {

    var mapCenter = new google.maps.LatLng(37.891586, -4.7844853); //Córdoba
    var map;
    var poly;
    var markers = [];
    var Marcadores = [];
    var path;
    var infoWindows = [];
    var edit = false;
    var selectedMarker;

    // load map
    var geocoder;
    var opt = {
        lines: 11, // The number of lines to draw
        length: 40, // The length of each line
        width: 10, // The line thickness
        radius: 50, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 58, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1.3, // Rounds per second
        trail: 76, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent in px
        // Left position relative to parent in px
    }
    var polyOptions = {
        strokeColor: '#000000',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        editable: false,
        draggable: false
    };

    map_initialize();


    $('#descriptionRoute').editable({
        inlineMode: false,
        zIndex: 2001,
        height: 500,
    });

    function map_initialize() {
        //Google map option

        geocoder = new google.maps.Geocoder();
        var googleMapOptions =
                {
                    center: mapCenter, // map center
                    zoom: 15, //zoom level, 0 = earth view to higher value
                    panControl: true, //enable pan Control
                    zoomControl: true, //enable zoom control
                    disableDefaultUI: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.SMALL //zoom control size
                    },
                    scaleControl: true, // enable scale control
                    mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
                };

        var mapDiv = document.getElementById("google_map");
        map = new google.maps.Map(mapDiv, googleMapOptions);

        var controlBtns = document.createElement('div');
        var seachFormDiv = document.createElement('div');
        var homeControl = new ControlsMap(controlBtns, map);
        var searchForm = new SearchBox(seachFormDiv, map);
        controlBtns.index = 1;
        seachFormDiv.index = 1;
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlBtns);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(seachFormDiv);

        google.maps.event.addListener(map, 'click', function(event) {
            addMarket(map, event.latLng, null, null, null, null);
        });
        /*
         * Opciones de la polilínea
         */

        poly = new google.maps.Polyline(polyOptions);
        poly.setMap(map);
        path = poly.getPath();
        var externalMarkers = getData();
        if (externalMarkers != 'new') {
            edit = true;
            createMarkers(externalMarkers);
        } else {
            markers = [];
            Marcadores = [];
            path = [];
            edit = false;
   poly = new google.maps.Polyline(polyOptions);
        poly.setMap(map);
        path = poly.getPath();
        var externalMarkers = getData();
        if (externalMarkers != 'new') {
            edit = true;
            createMarkers(externalMarkers);
        } else {
           
        }


        google.maps.event.addListener(poly, 'rightclick', function(event) {
            path.pop();
            var marker = markers.pop();
            marker.setMap(null);
        });

    }
    ;
    /*
     * Añade un marcador y asocia todas las funciones a él.
     * @param {google.map.Map} map
     * @param {google.maps.LatLng} latLng
     * @returns {undefined}
     */
    function addMarket(map, latLng, title, icon, description, createTime) {
        if ((typeof description === "undefined") || (description == null)) {
            description = "";
        }
        if ((typeof title === "undefined") || (title == null)) {
            title = "";
        }
        var marker = new google.maps.Marker({
            position: latLng, //map Coordinates where user right clicked
            map: map,
            draggable: false, //set marker draggable 
            animation: google.maps.Animation.DROP, //bounce animation
            title: latLng.toString(),
            icon: icon
                    //custom pin icon
        });
        path = poly.getPath();
        path.push(latLng);
        markers.push(marker);
        parseData(marker, name, description, '0');
        //Marcadores.push();
        var textBtn = "Incluir";
        var textDisabled = "disabled = true";
        if (icon != null && icon == "img/marker.png") {
            textBtn = "Incluir";
            textDisabled = "disabled = true";

        } else if (icon != null && icon == "img/pin_blue.png") {
            textBtn = "Excluir";
            textDisabled = "";
        }


        var contentString = $(
                '<div id="'+marker.getTitle()+'" class="marker-info-win">' +
                '<div class="marker-inner-win"><span class="info-content">' +
                '<h1 class="marker-heading">Punto de Ruta</h1>' +
                '' +
                '¿Añadir parada a la ruta?' +
                '</span>' +
                '<br />' +
                '<div class = "btn-group" >' +
                '<!--<button name="remove-marker" class="remove-marker btn btn-default" title="Remove Marker to Route">Borrar</button>-->' +
                '<button name="add-marker" class="add-marker btn btn-default" title="Add Marker to Route">' + textBtn + '</button>' +
                '<button id="edit-marker"' + textDisabled + ' name="edit-marker" class="edit-marker btn btn-primary" title="Editar más opciones marcador">Detalles</button>' +
                '</div>' +
                '</div></div>'
                );
        var infowindow = new google.maps.InfoWindow();
        infowindow.setContent(contentString[0]);
        //closeOtherAndOpenInfoWindow(infowindow, marker);
        google.maps.event.addListener(marker, 'click', function() {
            closeOtherAndOpenInfoWindow(infowindow, marker);
            selectedMarker = marker;
        });




        if (edit) {
            $('#title').attr('value', title);
            $('#descripcion').html(description);
        }



        /*var removeBtn = contentString.find('button.remove-marker')[0];
         google.maps.event.addDomListener(removeBtn, "click", function() {
         marker.setMap(null);
         var index = markers.indexOf(marker);
         markers.splice(index, 1);
         });*/
        var addBtn = contentString.find('button.add-marker')[0];
        google.maps.event.addDomListener(addBtn, "click", function() {
            if ($('.add-marker').html() == 'Excluir') {
                marker.setIcon("img/marker.png");
                parseData(marker, title, description, '0');
                $('.add-marker').html('Incluir');
                $('.edit-marker').prop("disabled", true);
            } else {

                $('.add-marker').html('Excluir');
                $('.edit-marker').prop("disabled", false);
                if (marker.getIcon() != "img/pin_blue.png") {
                    marker.setIcon("img/pin_blue.png");
                }
            }

        });
        var editBtn = contentString.find('button.edit-marker')[0];
        google.maps.event.addDomListener(editBtn, "click", function() {
            $('#des').editable({
                inlineMode: false,
                zIndex: 2001,
                height: 300,
            });
            $('#nameMarker').editable({
                inlineMode: true,
                zIndex: 2001,
                buttons: ["bold", "italic", "underline", "color"],
                placeholder: "Título del punto de parada"
            });
            $("#editMarker").modal({
                backdrop: 'static',
                show: true
            });

            $('#des').editable("setHTML", marker, false);






        });




    }
    ;

    function createMarkers(markers) {
        var markersJSON = eval('(' + markers + ')');
        console.log('Marcadores a mostrar: ' + markersJSON);
        for (var pro in markersJSON) {
            var createTime = markersJSON[pro]['CreateTime'];
            var description = markersJSON[pro]['Desciption'];
             console.log('Marcadores a mostrar: ' + markersJSON);
            var icon = markersJSON[pro]['Icon'];
            var lat = markersJSON[pro]['Lat'];
            var lng = markersJSON[pro]['Lng'];
            var order = markersJSON[pro]['Order'];
            var title = markersJSON[pro]['Title'];
            var pos = new google.maps.LatLng(lat, lng, true);
            addMarket(map, pos, title, icon, description, createTime);
        }
    }
    /*
     * Función que limpia la ruta y lo que haya guardado en ella
     * @returns {_L1.reiniciarTodo}
     */
    function reiniciarTodo() {
        $.each(markers, function(index, marker) {
            marker.setMap(null);
        });
        markers = [];
        Marcadores = [];
        path.clear();
    }
    function codeAddress() {
        var address = document.getElementById('adress').value;
        geocoder.geocode({'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                //path.push(results[0].geometry.location);
                //addMarket(map, results[0].geometry.location, null, null, null, null);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }
    /*
     * Función que añade el marcador añadido a un Array que puede ser mandado por Ajax. 
     * Para guardar los datos en la base de datos.
     * @param {google.maps.Marker} marker
     * @param {String} name
     * @param {String} description
     * @returns {undefined}
     */
    function parseData(marker, name, description, identificador) {

        function Marcador(id, marker, identificador, name, description, icon, lat, lng, title, order) {
            this.id = id;
            this.marker = marker;
            this.identificador = identificador;
            this.name = name;
            this.description = description;
            this.icon = icon;
            this.lat = lat;
            this.lng = lng;
            this.title = title;
            this.order = order;
        }
        var latLng = marker.getPosition();
        var id = marker.getTitle();
        var order = markers.indexOf(marker);
        var mark = new Marcador(id, marker, identificador, name, description, marker.getIcon(), latLng.lat(), latLng.lng(), name, order);
        if (arrayObjectIndexOf(Marcadores, id, "id") != -1) {
            Marcadores[arrayObjectIndexOf(Marcadores, id, "id")] = mark;
        } else {

            Marcadores.push(mark);
        }

    }
    /*
     * Función para mandar los datos de la ruta guardada en AJAX al servidor y guardarlo en la base de 
     * datos
     * @param {Array de Markers} markers
     * @returns {undefined}
     */
    function saveRoute(route) {
//Hacer que sobreescriba la ruta actual o que salve una nueva segun sea el caso
        var routeId = id;
        if (edit) {
            var action = "editMarkersRoute";
        } else {
            var action = "saveMarkers";

        }
        $.ajax({
            url: '../../build/controller.php',
            data: {
                ruta: route,
                routeId: id,
                action: action
            },
            type: 'POST',
            success: function(data) {
                alert(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }
    function saveMarkers(marcadores) {
//Hacer que sobreescriba la ruta actual o que salve una nueva segun sea el caso
        console.log("Id de la ruta mandado: " + id);
        var action = "storeMarkers";

        $.ajax({
            url: '../../build/controller.php',
            data: {
                marcadores: marcadores,
                routeId: id,
                action: action
            },
            type: 'POST',
            success: function(data) {
                alert(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError);
            }
        });
    }

    /*
     * Función que cierra todas las InfoWindows y abre la pulsada 
     */
    var closeOtherAndOpenInfoWindow = function(infowindow, marker) {
        infowindow.open(map, marker);
        if (infoWindows.length !== 0) {
            for (var i = 0; i < infoWindows.length; i++) {
                if (infoWindows[i] !== infowindow) {
                    infoWindows[i].close();
                }
            }

        }
        infoWindows.push(infowindow);
    };

    function ControlsMap(controlDiv, map) {
        controlDiv.style.padding = '5px';
        controlDiv.setAttribute('class', 'btn-group');
        //Creamos un botón
        var btnSave = document.createElement('button');
        btnSave.setAttribute('class', 'btn btn-default');
        btnSave.textContent = "Guardar";
        btnSave.setAttribute('id', 'saveBtn');
        controlDiv.appendChild(btnSave);
        var btnReset = document.createElement('button');
        btnReset.setAttribute('class', 'btn btn-danger');
        btnReset.textContent = "Reiniciar";
        btnReset.setAttribute('id', 'resetBtn');
        controlDiv.appendChild(btnReset);
        var btnCancel = document.createElement('button');
        btnCancel.setAttribute('class', 'btn btn-warning');
        btnCancel.textContent = "Cancelar";
        btnCancel.setAttribute('id', 'cancelBtn');
        controlDiv.appendChild(btnCancel);


        //Aquí ponemos los eventos de los botones

        google.maps.event.addDomListener(btnSave, 'click', function() {


            console.log("Marcadores mandados a guardar: " + Marcadores);
            saveMarkers(Marcadores);
        });
        google.maps.event.addDomListener(btnReset, 'click', function() {
            alert('Hacer Modal de confirmacion');
            reiniciarTodo();
        });
        google.maps.event.addDomListener(btnCancel, 'click', function() {
            alert('Confirmación de salir sin guardar');
            window.location = "desktop.php";
        });


    }
    function SearchBox(controlDiv, map) {
        controlDiv.style.padding = '5px';
        controlDiv.setAttribute('class', 'col-sm-8');

        var searchBox = document.createElement('form');
        searchBox.setAttribute('class', '');
        searchBox.setAttribute('role', 'search');

        var div1 = document.createElement('div');
        div1.setAttribute('class', 'form-group  col-sm-4');
        var inputSearch = document.createElement('input');
        inputSearch.setAttribute('id', 'adress');
        inputSearch.setAttribute('type', 'text');
        inputSearch.setAttribute('value', 'Córdoba');
        inputSearch.setAttribute('class', 'form-control');
        div1.appendChild(inputSearch);
        searchBox.appendChild(div1);
        var btnSearch = document.createElement('button');
        btnSearch.setAttribute('id', 'btnSearch');
        btnSearch.setAttribute('type', 'button');
        btnSearch.setAttribute('value', 'search');
        btnSearch.setAttribute('class', 'btn btn-primary');
        btnSearch.textContent = 'Centrar';

        controlDiv.appendChild(searchBox);
        controlDiv.appendChild(btnSearch);
        google.maps.event.addDomListener(btnSearch, 'click', function() {
            alert('Se centrará la vista donde hayas indicado');
            codeAddress();
        });
    }





    /*
     * 
     * Función que busca si existe un elemento en un array comparando por un elemento del Array
     */
    function arrayObjectIndexOf(myArray, searchTerm, property) {
        for (var i = 0, len = myArray.length; i < len; i++) {
            if (myArray[i][property] === searchTerm)
                return i;
        }
        return -1;
    }
}
});







