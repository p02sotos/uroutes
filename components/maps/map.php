<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/" . "Routes" . "/build/config.php";
include_once $config['pathsAbs']['loginDir'] . 'security.php';
include_once $config['pathsAbs']['controllerPage'];
if (isset($_SESSION['markers'])) {
    $markers = $_SESSION['markers'];
    $origin = $_SESSION['origin'];
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Crea tu ruta</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width">
        <?php include_once $config['pathsAbs']['templateDir'] . "header.php"; ?>
        <style type="text/css">
            html { height: 100% }
            body { height: 100%; margin: 0; padding: 0 }
        </style> 
        <script>
            function getData() {
                var obj = <?php echo json_encode($markers) ?>;
                return obj;
            }
            function getOrigin() {
                var obj = <?php echo json_encode($origin); ?>;
                return obj;
            }
        </script>
        <?php
        if (isset($id)) {
            echo "<script> id =  $id </script>";
        }
        ?>

        <script src="js/jquery.form.min.js"></script>
        <script src="js/spin.min.js"></script>
        <link href="editor/css/font-awesome.min.css" rel="stylesheet">
        <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css">
        <script src="editor/js/froala_editor.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/spin.js/1.2.7/spin.min.js"></script>
        <script src="js/map.js" ></script> 
        <script type="text/javascript"  src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAZCBJwQOs_mOtC-bn0coWK04QXc6ZDDmA&sensor=false"></script>

    </head>
    <body>      
        <div id="google_map" class="center-block" ></div>
        
        <?php include_once $config['pathsAbs']['templateDir'] . "editMarker.php"; ?>   
    </body>
</html>
