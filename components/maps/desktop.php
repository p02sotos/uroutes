<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/" . "Routes" . "/build/config.php";
include_once $config['pathsAbs']['loginDir'] . 'security.php';
if (isset($_SESSION['routes'])) {
    $data = $_SESSION['routes'];
}
if ($data != "null") {
    $dataArray = json_decode($data, true);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Routes</title>
        <?php include_once $config['pathsAbs']['templateDir'] . "header.php"; ?>
        <link href="editor/css/font-awesome.min.css" rel="stylesheet">
        <link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css">
        <script src="editor/js/froala_editor.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/spin.js/1.2.7/spin.min.js"></script>
        <script src="<?php echo $config['paths']['componentsDir'] ?>jquery/other/freewall.js"></script>
        <script>  
            var datas = <?php echo json_encode($dataArray); ?>;
            console.log("datas-->" + datas);

        </script>



    </head>
    <body>
        <div>
            <!-- CABECERA -->
            <div id="head">
                <div class="container">
                    <div class="row">
                        <div  class="col-sm-12">
                            <?php include_once $config['pathsAbs']['templateDir'] . "head.php"; ?>   
                        </div>
                    </div>
                </div>
            </div>
            <!-- CUERPO -->
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div> 

                                <?php
                                include_once $config['pathsAbs']['templateDir'] . "genericTableRoutes.php";
                                ?>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FOOTER -->
            <div id="foot">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <?php include_once $config['pathsAbs']['templateDir'] . "footer.php"; ?>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include_once $config['pathsAbs']['templateDir'] . "confirmModal.php"; ?>   
        <?php include_once $config['pathsAbs']['templateDir'] . "waitingModal.php"; ?>
        <?php include_once $config['pathsAbs']['templateDir'] . "editRoute.php"; ?>
        <script src="<?php echo $config['paths']['componentsDir'] ?>desktop.js"></script>
    </body>
</html>


